# excel-sheets

Hey, there 👋

This project is all about excel sheets.

## Installing Tools


### Our Recommendations
- Use **latest version** of excel


## sheets

### excel-calculators
- 01 - EMI Calculator
- 02 - SIP and Lumpsum Calculator

### excel-templates
- 01 - Students Attendance Record

### master-excel
- 01 - Introduction to excel
